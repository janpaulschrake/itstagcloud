<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'itstagcloud',
    'itstagcloud',
    'IT-Schrake Tag-Cloud',
    'EXT:itstagcloud/Resources/Public/Icons/ext_icon.png'
);

/**
 * Remove unused fields
 */
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['itstagcloud_itstagcloud'] = 'recursive,pages';

/**
 * Add Flexform for itstagcloud plugin
 */
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['itstagcloud_itstagcloud'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'itstagcloud_itstagcloud',
    'FILE:EXT:itstagcloud/Configuration/FlexForm/resultPage.xml'
);