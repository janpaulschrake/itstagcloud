<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'IT-Schrake Tag-Cloud',
    'description' => 'An extension to generate tagcloud out of text links, table data from ke_search ',
    'category' => 'plugin',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Bülent Aldede',
    'author_email' => 'aldede@it-schrake.net',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.1-9.5.99',
            'ke_search' => '2.8.1',
            
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'Itschrake\\Itstagcloud\\' => 'Classes'
        ],
    ],
];
