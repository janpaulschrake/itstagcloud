<?php

/**
 * @author Bülent Aldede 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Itschrake.itstagcloud',
            'itstagcloud',
            [
                'Tag' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Tag' => 'list, show'
            ]
        );
    }
);
